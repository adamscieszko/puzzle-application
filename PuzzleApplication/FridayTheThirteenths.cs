﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PuzzleApplication
{
    //Friday the Thirteenths
    //The goal is to identify all friday the thirteenths for a given timeframe
    class FridayTheThirteenths
    {
        public IEnumerable<DateTime> Execute(DateTime startDate, DateTime endDate)
        {
            var fridays = new List<DateTime>();
            if (endDate < startDate)
                throw new ArgumentException("End can't be lower than start date");
            var firstMonth = startDate.Day > 13 ? new DateTime(startDate.Year, startDate.AddMonths(1).Month + 1, 13) : new DateTime(startDate.Year, startDate.Month, 13);
            var lastMonth = endDate.Day < 13 ? new DateTime(endDate.Year, endDate.AddMonths(-1).Month, 13) : new DateTime(endDate.Year, endDate.Month, 13);
            var tmpDate = firstMonth;
            do
            {
                if (tmpDate.DayOfWeek == DayOfWeek.Friday)
                    fridays.Add(tmpDate);
                tmpDate = tmpDate.AddMonths(1);
            } while (tmpDate <= lastMonth);

            return fridays;
        }
    }
}
